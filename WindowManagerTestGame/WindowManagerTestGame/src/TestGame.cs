using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace WindowManager
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    class TestGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        WindowManager wm;
        BasicEffect effect;
        float xPosition = 0;
        float boxX, boxY;

        public TestGame()
        {
            graphics = new GraphicsDeviceManager(this);
            wm = new WindowManager(graphics);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            localInit();
            wm.initSettings();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            KeyboardState keyboard = Keyboard.GetState();
            if (keyboard.IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }
            // TODO: Add your update logic here
            doMouseStuff();
            xPosition += gameTime.ElapsedGameTime.Ticks / 100000f;
            if (xPosition > 100) xPosition = 0;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();

                drawBox();
                drawLine();
            }

            // System.Threading.Thread.Sleep(20);
            base.Draw(gameTime);
        }

        private void localInit()
        {
            initEffect();
            this.IsMouseVisible = true;
            // 10,000 TimeSpan units is one ms.
            // 60 fps = 16.6666... ms per frame.
            // So, we want one frame to be 166666 units.
            // this.TargetElapsedTime = new TimeSpan(1);
            this.IsFixedTimeStep = false;
        }

        private void doMouseStuff()
        {
            MouseState mouse = Mouse.GetState();
            boxX = mouse.X * 100f / GraphicsDevice.Viewport.Width;
            boxY = mouse.Y * 100f / GraphicsDevice.Viewport.Height;
        }

        private void initEffect()
        {
            effect = new BasicEffect(GraphicsDevice);
            effect.VertexColorEnabled = true;
            effect.World = Matrix.CreateTranslation(0, 0, 0);
            effect.View = Matrix.CreateLookAt(new Vector3(0, 0, 1), Vector3.Zero, Vector3.Up);
            effect.Projection = Matrix.CreateOrthographicOffCenter(0, 100, 100, 0, 1, 1000);
        }

        private void drawLine()
        {
            VertexPositionColor[] vertexData = new VertexPositionColor[2];
            vertexData[0] = new VertexPositionColor(new Vector3(xPosition, 0, 0), Color.White);
            vertexData[1] = new VertexPositionColor(new Vector3(xPosition, 100, 0), Color.White);
            int[] indexes = new int[2];
            indexes[0] = 0;
            indexes[1] = 1;
            GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, vertexData, 0, 2, indexes, 0, 1);
        }

        private void drawBox()
        {
            VertexPositionColor[] vertexData = new VertexPositionColor[4];
            vertexData[0] = new VertexPositionColor(new Vector3(boxX-1, boxY-1, 0), Color.White);
            vertexData[1] = new VertexPositionColor(new Vector3(boxX, boxY-1, 0), Color.White);
            vertexData[2] = new VertexPositionColor(new Vector3(boxX-1, boxY, 0), Color.White);
            vertexData[3] = new VertexPositionColor(new Vector3(boxX, boxY, 0), Color.White);
            int[] indexes = new int[4];
            indexes[0] = 0;
            indexes[1] = 1;
            indexes[2] = 2;
            indexes[3] = 3;
            RasterizerState raster = new RasterizerState();
            raster.FillMode = FillMode.Solid;
            raster.CullMode = CullMode.None;
            GraphicsDevice.RasterizerState = raster;
            GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.TriangleStrip, vertexData, 0, 4, indexes, 0, 2);
        }
    }
}
