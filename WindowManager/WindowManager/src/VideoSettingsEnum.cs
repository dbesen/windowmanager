﻿namespace WindowManager
{
    public enum VideoSettingsEnum
    {
        FULLSCREEN,
        SCREEN_WIDTH,
        SCREEN_HEIGHT,
        VSYNC
    }
}