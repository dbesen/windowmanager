﻿using Microsoft.Xna.Framework;
using SettingsManager;

namespace WindowManager
{
    public class WindowManager
    {
        GraphicsDeviceManager graphics;

        public WindowManager(GraphicsDeviceManager graphics)
        {
            this.graphics = graphics;
        }

        public void initSettings()
        {
            SettingsManager.SettingsManager<VideoSettingsEnum> sm = new SettingsManager.SettingsManager<VideoSettingsEnum>();
            setDefaultSettings(sm);
            loadSettings(sm);
            applySettings(sm);
        }

        private void applySettings(SettingsManager.SettingsManager<VideoSettingsEnum> sm)
        {
            BasicSetting<bool> fs = sm.getSetting<BasicSetting<bool>>(VideoSettingsEnum.FULLSCREEN);
            BasicSetting<int> width = sm.getSetting<BasicSetting<int>>(VideoSettingsEnum.SCREEN_WIDTH);
            BasicSetting<int> height = sm.getSetting<BasicSetting<int>>(VideoSettingsEnum.SCREEN_HEIGHT);
            BasicSetting<bool> vsync = sm.getSetting<BasicSetting<bool>>(VideoSettingsEnum.VSYNC);
            if (fs.value)
            {
                this.graphics.PreferredBackBufferWidth = width.value;
                this.graphics.PreferredBackBufferHeight = height.value;
                this.graphics.IsFullScreen = true;
            }
            else
            {
                this.graphics.IsFullScreen = false;
            }
            graphics.SynchronizeWithVerticalRetrace = vsync.value;
            graphics.ApplyChanges();
        }

        private static void loadSettings(SettingsManager.SettingsManager<VideoSettingsEnum> sm)
        {
            // Load from file
            sm.readFileIfExists("video.cfg");

            // Write any settings changes back out (new/deleted setting keys)
            sm.writeFile("video.cfg");
        }

        private void setDefaultSettings(SettingsManager.SettingsManager<VideoSettingsEnum> sm)
        {
            sm.setSetting(VideoSettingsEnum.FULLSCREEN, new BasicSetting<bool>(false));
            sm.setSetting(VideoSettingsEnum.SCREEN_WIDTH, new BasicSetting<int>(this.graphics.GraphicsDevice.DisplayMode.Width));
            sm.setSetting(VideoSettingsEnum.SCREEN_HEIGHT, new BasicSetting<int>(this.graphics.GraphicsDevice.DisplayMode.Height));
            sm.setSetting(VideoSettingsEnum.VSYNC, new BasicSetting<bool>(true));
        }

    }
}
