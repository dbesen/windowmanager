﻿General todo:
1. Add vsync
2. Add FPS and current resolution display, bound to settings
3. Aspect ratio management?

For vsync:
(done) 1. Write a visual vsync test
(done) 2. Turn on/off vsync (bound to a setting)
(done) 3. Test if vsync works both windowed and fullscreen - works fullscreen, windowed it's forced to the aero setting
(done) 4. Write a mouse lag test
5. Test mouse lag with vsync on and try to avoid it automatically
 - See mouselag.xlsx
 - Post on the XNA forums about it (ugh).
6. If no solution for mouse lag is found, stop using XNA and switch to OpenGL.
